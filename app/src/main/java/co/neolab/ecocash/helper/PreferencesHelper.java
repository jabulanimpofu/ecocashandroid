package co.neolab.ecocash.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import co.neolab.ecocash.model.UserModel;

/**
 * Created by jabulanimpofu on 28/6/2017.
 */

public class PreferencesHelper {
    private static final String PLAYER_PREFERENCES = "userPreferences";
    private static final String PREFERENCE_PHONE_NUMBER = PLAYER_PREFERENCES + ".phoneNumber";

    private PreferencesHelper() {
        //no instance
    }

    /**
     * Writes a {@link co.neolab.ecocash.model.UserModel} to preferences.
     *
     * @param context The Context which to obtain the SharedPreferences from.
     * @param user  The {@link co.neolab.ecocash.model.UserModel} to write.
     */
    public static void writeToPreferences(Context context, UserModel user) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString(PREFERENCE_PHONE_NUMBER, user.getPhoneNumber());
        editor.apply();
    }


    /**
     * Retrieves a {@link co.neolab.ecocash.model.UserModel} from preferences.
     *
     * @param context The Context which to obtain the SharedPreferences from.
     * @return A previously saved player or <code>null</code> if none was saved previously.
     */
    public static UserModel getUser(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        final String phoneNumber = preferences.getString(PREFERENCE_PHONE_NUMBER, null);

        if (null == phoneNumber) {
            return null;
        }
        return new UserModel(phoneNumber);
    }


    /**
     * Signs out a player by removing all it's data.
     *
     * @param context The context which to obtain the SharedPreferences from.
     */
    public static void signOut(Context context) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.remove(PREFERENCE_PHONE_NUMBER);
        editor.apply();
    }

    /**
     * Checks whether a player is currently signed in.
     *
     * @param context The context to check this in.
     * @return <code>true</code> if login data exists, else <code>false</code>.
     */
    public static boolean isSignedIn(Context context) {
        final SharedPreferences preferences = getSharedPreferences(context);
        return preferences.contains(PREFERENCE_PHONE_NUMBER);
    }

    /**
     * Checks whether the player's input data is valid.
     *
     * @param firstName   The player's first name to be examined.
     * @param lastInitial The player's last initial to be examined.
     * @return <code>true</code> if both strings are not null nor 0-length, else <code>false</code>.
     */
    public static boolean isInputDataValid(CharSequence firstName, CharSequence lastInitial) {
        return !TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastInitial);
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PLAYER_PREFERENCES, Context.MODE_PRIVATE);
    }
}
