package co.neolab.ecocash.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jabulanimpofu on 28/6/2017.
 */

public class UserModel implements Parcelable {

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
    private final String mPhoneNumber;

    public UserModel(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    protected UserModel(Parcel in) {
        mPhoneNumber = in.readString();
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mPhoneNumber);
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserModel player = (UserModel) o;

        if (!mPhoneNumber.equals(player.mPhoneNumber)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = mPhoneNumber.hashCode();
        return result;
    }
}
