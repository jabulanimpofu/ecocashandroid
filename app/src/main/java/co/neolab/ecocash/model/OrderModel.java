package co.neolab.ecocash.model;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderModel implements Parcelable {
    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel in) {
            return new OrderModel(in);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };
    private final String mAmount;
    private final String mOrderId;
    private final String mDescription;
    private final String mPaymentStatusCode;
    private final String mPaymentId;

    public OrderModel(String amount, String description, String orderId, String paymentStatusCode, String paymentId) {
        mAmount = amount;
        mDescription = description;
        mOrderId = orderId;
        mPaymentStatusCode = paymentStatusCode;
        mPaymentId = paymentId;
    }

    public OrderModel(String amount, String description, String orderId, String paymentStatusCode) {
        mAmount = amount;
        mDescription = description;
        mOrderId = orderId;
        mPaymentStatusCode = paymentStatusCode;
        mPaymentId = null;
    }

    public OrderModel(String amount, String description) {
        mAmount = amount;
        mDescription = description;
        mOrderId = null;
        mPaymentStatusCode = null;
        mPaymentId = null;
    }

    protected OrderModel(Parcel in) {
        mAmount = in.readString();
        mDescription = in.readString();
        mOrderId = in.readString();
        mPaymentStatusCode = in.readString();
        mPaymentId = in.readString();
    }

    public String getAmount() {
        return mAmount;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getOrderId(){
        return mOrderId;
    }

    public String getPaymentStatusCode () {
        return mPaymentStatusCode;
    }

    public String getPaymentId () {
        return mPaymentId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAmount);
        dest.writeString(mDescription);
        dest.writeString(mOrderId);
        dest.writeString(mPaymentStatusCode);
        dest.writeString(mPaymentId);
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderModel player = (OrderModel) o;

        if (!mAmount.equals(player.mAmount)) {
            return false;
        }

        if (!mDescription.equals(player.mDescription)) {
            return false;
        }

        if (!mOrderId.equals(player.mOrderId)) {
            return false;
        }

        if (!mPaymentStatusCode.equals(player.mPaymentStatusCode)) {
            return false;
        }

        if (!mPaymentId.equals(player.mPaymentId)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = mAmount.hashCode();
        return result;
    }

    public String toString() {
        return String.format("Order {amount: %s, description: %s, orderId: %s, paymentStatusCode: %s, paymentId: %s}", mAmount, mDescription, mOrderId, mPaymentStatusCode, mPaymentId);
    }
}
