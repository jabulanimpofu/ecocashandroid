package co.neolab.ecocash.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsReceiver extends BroadcastReceiver {
    public static final String SAMPLE_SMS = "You have successfully paid USD0.50 to ROAD RULES SOLUTIONS (88782) Merchant. Txn ID MP170308.1639.D06863. New wallet balance is USD10.13.";
    public static final String INCOMING_SMS_SENDER_NUMBERS[] = {"+263164"};
    public static final String REGEX = "(USD\\d*\\.{1}\\d{2}).*(\\(\\d{5}\\)).*(MP\\d*.{1}\\d{4}.[a-zA-Z]\\d{5})";
    public static final String INTENT_ACTION_SMS = "intent_action_sms";
    public static final String KEY_SMS_SENDER = "key_sms_sender";
    public static final String KEY_SMS_MESSAGE = "key_sms_message";
    public static final String KEY_SMS_AMOUNT_PAID = "key_sms_message";
    public static final String KEY_SMS_MERCHANT_CODE = "key_sms_message";
    public static final String KEY_SMS_TXN_ID = "key_sms_message";

    private static final String INTENT_ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(INTENT_ACTION_SMS_RECEIVED) || INCOMING_SMS_SENDER_NUMBERS == null) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }

        List<String> smsSenderNumbers = Arrays.asList(INCOMING_SMS_SENDER_NUMBERS);
        SmsMessage[] smsMessages;
        String messageFrom = null;


        //PDU = protocol data unit
        //A PDU is a “protocol data unit”, which is the industry format for an SMS message.
        //Because SMSMessage reads/writes them you shouldn't need to dissect them.
        //A large message might be broken into many, which is why it is an array of objects.
        Object[] pdus = (Object[]) bundle.get("pdus");
        if (pdus == null) {
            return;
        }


        smsMessages = new SmsMessage[pdus.length];
        // If the sent message is longer than 160 characters  it will be broken down
        // in to chunks of 153 characters before being received on the device.
        // To rectify that receivedMessage is the result of appending every single
        // short message into one large one for our usage. see:
        //http://www.textanywhere.net/faq/is-there-a-maximum-sms-message-length

        for (int i = 0; i < smsMessages.length; i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdus[i],
                        bundle.getString("format"));
            } else {
                smsMessages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            messageFrom = smsMessages[i].getOriginatingAddress();
        }

        if (TextUtils.isEmpty(messageFrom) || !smsSenderNumbers.contains(messageFrom)) {
            return;
        }

        String receivedMessage = "";
        for (SmsMessage smsMessage : smsMessages) {
            receivedMessage += smsMessage.getMessageBody();
        }

        // Create a Pattern object
        Pattern r = Pattern.compile(REGEX);
        // Now create matcher object.
        Matcher m = r.matcher(receivedMessage);

        if(!m.find()){
            return;
        }

        String receivedAmountPaid = m.group(0);
        String receivedMerchantCode = m.group(1);
        String receivedTxnId = m.group(2);
        sendBroadcast(context, messageFrom, receivedMessage, receivedAmountPaid, receivedMerchantCode, receivedTxnId);
    }

    private void sendBroadcast(@NonNull Context context,
                               @Nullable String messageFrom,
                               @Nullable String smsMessage,
                               @Nullable String receivedAmountPaid,
                               @Nullable String receivedMerchantCode,
                               @Nullable String receivedTxnId) {

        Intent broadcastIntent = new Intent(INTENT_ACTION_SMS);
        broadcastIntent.putExtra(KEY_SMS_SENDER, messageFrom);
        broadcastIntent.putExtra(KEY_SMS_MESSAGE, smsMessage);
        broadcastIntent.putExtra(KEY_SMS_AMOUNT_PAID, receivedAmountPaid);
        broadcastIntent.putExtra(KEY_SMS_MERCHANT_CODE, receivedMerchantCode);
        broadcastIntent.putExtra(KEY_SMS_TXN_ID, receivedTxnId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }
}
