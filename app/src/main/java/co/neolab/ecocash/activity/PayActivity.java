package co.neolab.ecocash.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.beardedhen.androidbootstrap.BootstrapLabel;

import co.neolab.ecocash.R;
import co.neolab.ecocash.model.OrderModel;

public class PayActivity extends AppCompatActivity {

    public static final String TAG = PayActivity.class.getSimpleName();

    private BootstrapLabel mStatus;
    private TextView mInstruction;
    private BootstrapEditText mAmount;
    private BootstrapEditText mDescription;
    private BootstrapButton mPay;
    private OrderModel mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);

        mPay = (BootstrapButton) findViewById(R.id.pay_submit);
        mPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makePayment();
            }
        });

        mInstruction = (TextView) findViewById(R.id.pay_instruction);
    }

    private void makePayment() {
        mAmount = (BootstrapEditText) findViewById(R.id.pay_amount);
        mDescription = (BootstrapEditText) findViewById(R.id.pay_description);

        String amount;
        String description;

        try {
            amount = "" + Integer.parseInt(mAmount.getText().toString());
        } catch (Exception e) {
            Log.e(TAG, "An invalid or non integral value was given");
            return;
        }
        try {
            description = mDescription.getText().toString();
        } catch (Exception e) {
            Log.e(TAG, "An error retrieving the description");
            return;
        }
    }
}
